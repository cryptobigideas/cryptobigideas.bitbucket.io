L = {
    'a':'Caesar encrypt and decrypt',
    'b':"""Caesar brute-force attack <i>(for teachers)</i>""",
    'c':'Caesar mysterious text to decrypt',
    'd':"""Caesar homework quick challenge <i>(decrypting an encrypted message posted on Google Classroom)</i>""",
    'e':'Caesar frequency attack',
    'f':'One-Time Pad encrypt, decrypt, and frequency attack',
    'g':'One-Time Pad brute-force attack'
    }

for lettera,titolo in L.items():
    s = f"""
<h3>{titolo} <button id="{lettera}" onclick="myFunction(this)">Hide</button></h3>
<p><a href="./snap/snap.html#open:../../cryptosnaps/{lettera}.xml">(open externally)</a></p>
<iframe title="Inline Frame Example" width="1024" height="720" id="{lettera}_frame"
    src="./snap/snap.html#open:../../cryptosnaps/{lettera}.xml&noExitWarning">
    Snap! frame not loaded
    correctly.</iframe>
<hr>
"""
    print(s)
    
    #https://cryptobigideas.bitbucket.io